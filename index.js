const http = require('http'),
    Sniffr = require("sniffr");

const requestListener = function (req, res) {
    const s = new Sniffr(),
        source = req.headers['user-agent'],
        ua = s.sniff(source),
        d = new Date();
    let link;

    console.log(`${d.toString()} : access on ${ua.os.name} ${ua.os.versionString} with ${ua.browser.name} browser.`);

    switch (ua.os.name) {
        case "ios":
            link = "https://apps.apple.com/id/app/kesan/id1479939730?l=id";
            break;
        case "android":
            link = "https://play.google.com/store/apps/details?id=com.kesan.android";
            break;
        default:
            res.writeHead(200);
            res.end(`I'm sorry, KESAN APP does'nt support on ${ua.os.name} ${ua.os.versionString} :)`);
            return;
    }

    res.writeHead(302, {
        'Location': link
    });
    res.end();
}

const server = http.createServer(requestListener);
console.log("Server ready on http://localhost:8080");
server.listen(8080);
